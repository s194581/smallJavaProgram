Feature: Add Numbers
  Add numbers together

  Scenario: adding two numbers
    Given I have a calculator
    When I add 2 and 2
    Then the sum is 4

  Scenario: adding no numbers
    Given I have a calculator
    When I add 0
    Then the sum is 0

  Scenario: adding many numbers
    Given I have a calculator
    When I add the numbers "1,1,1,1,1"
    Then the sum is 5

  Scenario: Adding by newline
    Given I have a calculator
    When I add numbers separated by newline, such as "1\n2,3"
    Then the sum is 6

  Scenario: Adding by other delimiters
    Given I have a calculator
    When I add numbers separated by another delimiter, such as "//;\n1;2;4"
    Then the sum is 7

  Scenario: Adding negative numbers
    Given I have a calculator
    When I add negative numbers like the string "6,-3,-4"
    Then the error message "negatives not allowed: -3 -4" is thrown

