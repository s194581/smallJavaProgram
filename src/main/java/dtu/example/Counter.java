package dtu.example;

/*
	Remove the class from your project as you write
	your own code.
 */
public class Counter {

	private int counter = 0;

	public void setCounter(int i) {
		counter = i;
	}

	public void inc() {
		counter++;
	}

	public int getCounter() {
		return counter;
	}

}
