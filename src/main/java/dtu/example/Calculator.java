package dtu.example;
import java.util.stream.DoubleStream;

public class Calculator {

    static double add(String numbers) throws Exception {
        int sum = 0;
        int start = 0;
        String exceptString = "negatives not allowed:";
        boolean except = false;
        String[] nums = numbers.split(",|\n");
        if(numbers.length()>=1) {
            if (nums[0].substring(0, 1).equals("/")) {
                String delim = nums[0].substring(2, 3);
                nums = numbers.split(delim + "|\n");
                start = 2;

            }
            for(int i=start;i<nums.length;i++){
                int num = Integer.parseInt(nums[i]);
                if(num<0){
                    except = true;
                    exceptString = exceptString+" "+nums[i];
                }
                sum+=num;
            }
            if(except == true){
                throw new Exception(exceptString);
            }
        }
        return sum;
    }


}
