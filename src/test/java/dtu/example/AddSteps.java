package dtu.example;
import org.junit.jupiter.api.Assertions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddSteps {
    Calculator cal;
    double sum;
    ErrorMessageHolder errorMessageHolder;

    public AddSteps(ErrorMessageHolder errorMessageHolder){
        this.errorMessageHolder = errorMessageHolder;
    }

    @Given("I have a calculator")
    public void i_have_a_calculator() {
        // Write code here that turns the phrase above into concrete actions
        cal = new Calculator();
        sum = 0;
    }

    @When("I add {int}")
    public void i_add(Integer int1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        sum = cal.add(int1.toString());
    }

    @When("I add {int} and {int}")
    public void i_add_and(Integer int1, Integer int2) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        sum = cal.add(int1+","+int2);

    }

    @When("I add the numbers {string}")
    public void iAddTheNumbers(String string) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        sum = cal.add(string);
    }

    @When("I add numbers separated by newline, such as {string}")
    public void iAddNumbersSeparatedByNewlineSuchAs(String string) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String s = string.replace("\\n","\n");
        sum = cal.add(s);
    }

    @When("I add numbers separated by another delimiter, such as {string}")
    public void iAddNumbersSeparatedByAnotherDelimiterSuchAs(String string) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String s = string.replace("\\n","\n");
        sum = cal.add(s);
    }

    @When("I add negative numbers like the string {string}")
    public void iAddNegativeNumbersLikeTheString(String string) {
        // Write code here that turns the phrase above into concrete actions
        try {
            String s = string.replace("\\n","\n");
            sum = cal.add(s);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }

    @Then("the error message {string} is thrown")
    public void theErrorMessageIsThrown(String string) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(string,errorMessageHolder.getErrorMessage());
    }

    @Then("the sum is {int}")
    public void theSumIs(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(sum, (double) int1);
    }



}
